const path              = require('path');
const UglifyJsPlugin    = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const BUILD_DIR = path.resolve(__dirname, 'public');
const APP_DIR   = path.resolve(__dirname, 'src');

module.exports = {
    entry: `${APP_DIR}/index.js`,
    output: {
        path: BUILD_DIR,
        publicPath: BUILD_DIR,
        filename: 'js/bundle.js'
    },
    module : {
        rules: [
            {
                test : /\.js$/,
                include : APP_DIR,
                loader : 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true,
                            discardComments: {
                                removeAll: true
                            }
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            minimize: true,
                            discardComments: {
                                removeAll: true
                            }
                        }
                    }]
                })
            }
        ]
    },
    plugins: [
        new UglifyJsPlugin(),
        new ExtractTextPlugin({
            filename: 'css/style.css',
        })
    ]
};