const apiUri = 'http://127.0.0.1:3000';

const getQueryString = (params) => {
    let esc = encodeURIComponent;
    return Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
};

const request = async (URL, data, params) => {
    let method = params.method || 'GET';
    let qs = '';
    let body;
    let headers = params.headers || {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };

    if (['GET', 'DELETE'].indexOf(method) > -1) {
        qs = (Object.keys(data).length) ? '?' + getQueryString(data) : '';
    }
    else {
        body = JSON.stringify(data);
    }

    let url = apiUri + URL + qs;
    let result = false;

    try {
        result = await fetch(url, {method, headers, body});
        result = result.json();
    }
    catch (e) {
        result = e;
    }

    return result;
};

export async function Get(URL, data = {}, params = {}) {
    return request(URL, data, Object.assign({method: 'GET'}, params || {})).catch((e) => {
        return e;
    });
}

export async function Post(URL, data = {}, params = {}) {
    return request(URL, data, Object.assign({method: 'POST'}, params || {})).catch((e) => {
        return e;
    });
}