import React from "react";
import {DebounceInput} from 'react-debounce-input';
import Form from './Form'
import AppLoader from './Loader';
import Comment from './Comment';
import { Get } from "../../Helper/Api";
import Popup from "./Popup";

export default class Feed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            listPlaceHolder: null,
            popup: false
        };

        this.togglePopup = this.togglePopup.bind(this);
        this.getComments = this.getComments.bind(this)
    }
    async componentDidMount() {
        await this.getComments();
    }
    async getComments(filter = 'all') {
        if(filter && filter.target) {
            filter = filter.target.value || 'all';
        }

        this.setState({listPlaceHolder: <AppLoader />});
        const comments = await Get(`/comment/${filter}`);

        if(comments && comments.code === 200) {
            this.setState({comments: comments.data});
        }
        else {
            this.setState({
                comments: [],
                listPlaceHolder: <h3>{comments.data}</h3>
            });
        }
    }
    async getUserLastComment(userEmail) {
        const comment = await Get(`/comment/last/${userEmail}`);

        if(comment && comment.code === 200) {
            let commentDate = new Date(comment.data);

            let details = {
                title: userEmail,
                body: commentDate.toLocaleString()
            };

            this.togglePopup(details);
        }
    }
    togglePopup(message) {
        if(message && message.title) {
            this.setState({popup: message});
        }
        else {
            this.setState({popup: false});
        }
    }
    render() {
        let comments = this.state.comments.map((comment, index) => {
            return <Comment
                key={index}
                gravatar={comment.gravatar}
                title={comment.user_email}
                text={comment.text}
                user_last_comment={this.getUserLastComment.bind(this,comment.user_email)}
            />
        });

        return (
            <div id="feed-container">
                <Form updateFeeds={this.getComments.bind(this)}/>
                <div id="comments-container">
                    <DebounceInput
                        minLength={0}
                        debounceTimeout={300}
                        onChange={this.getComments.bind(this)}
                        placeholder="Filter.."
                    />
                    {(comments.length) ? comments : this.state.listPlaceHolder}
                    {(this.state.popup) ? <Popup title={this.state.popup.title} body={this.state.popup.body} close={this.togglePopup}/> : ''}
                </div>
            </div>
        );
    }
}