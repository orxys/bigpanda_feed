import React from "react";
import Loader from 'react-loader-spinner'

export default class AppLoader extends React.Component {
    render() {
        return (
            <div className="loader-container">
                <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height="40"
                    width="40"
                />
            </div>
        );
    }
}