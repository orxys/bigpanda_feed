import React from "react";
import Popup from "./Popup"
import { Post } from "../../Helper/Api";

export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            comment: '',
            popup: false
        };
    }
    onFormUpdate(event) {
        const name  = event.target.name;
        const value = event.target.value;
        const state = this.state;
        state[name] = value;
        this.setState(state);
    }
    async handleFormSubmit(event) {
        event.preventDefault();

        if(this.state.email && this.state.comment) {
            try {
                const newComment = await Post('/comment',this.state);

                if(newComment && newComment.data) {

                    if(newComment.code === 200) {
                        this.props.updateFeeds();
                    }

                    this.setState({
                        popup: {
                            title: (newComment.code === 200) ? 'Success' : 'Ops..',
                            body: newComment.data
                        }
                    }, () => {
                        setTimeout(() => {
                            this.setState({popup: false});
                        },2500)
                    });
                }
            }
            catch (e) {
                console.log(e);
            }
        }
    }
    render() {
        return (
            <div id="form-container">
                <form onSubmit={this.handleFormSubmit.bind(this)}>
                    <input type="text" name="email" placeholder="Email" onChange={this.onFormUpdate.bind(this)}/>
                    <textarea name="comment" placeholder="Message" onChange={this.onFormUpdate.bind(this)}/>
                    <button type="submit">Submit</button>
                </form>
                {(this.state.popup) ? <Popup title={this.state.popup.title} body={this.state.popup.body} close={false}/> : ''}
            </div>
        );
    }
}