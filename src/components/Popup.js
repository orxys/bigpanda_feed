import React from "react";

export default class Popup extends React.Component {
    render() {
        return (
            <div id="popup-wrapper">
                <div id="popup-backdrop"/>
                <div id="popup-container">
                    <h3>{this.props.title}</h3>
                    <hr />
                    <p>
                        {this.props.body}
                    </p>
                    {(this.props.close) ?
                        <div>
                            <span id="close-popup" onClick={this.props.close}>Close</span>
                        </div> : ''
                    }
                </div>
            </div>
        );
    }
}