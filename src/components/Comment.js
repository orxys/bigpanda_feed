import React from "react";

export default class Comment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: null,
            email: null,
            text: null
        };
    }

    render() {
        return (
            <div className="comment">
                <div className="gravater" onClick={this.props.user_last_comment}>
                    <img src={this.props.gravatar}/>
                </div>
                <div className="content">
                    <h3>{this.props.title}</h3>
                    <span>{this.props.text}</span>
                </div>
            </div>
        );
    }
}