import React from 'react';
import ReactDOM from 'react-dom';
import Feed from './components/Feed'

import './styles/style.scss';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            updateFeed: false
        };
    }

    render() {
        return (
            <Feed />
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('app'));