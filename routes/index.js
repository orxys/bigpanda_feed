const express = require('express');
const router = express.Router();

router.get('/', function(Request, Response) {
    Response.render('index');
});

module.exports = router;
