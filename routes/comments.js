const express   = require('express');
const md5       = require('md5');
const User      = require('../models/User');
const Comment   = require('../models/Comment');
const router    = express.Router();

router.post('/', async (Request, Response) => {
    let result;
    const comment = {
        email: Request.body.email,
        content: Request.body.comment
    };

    result = await storeComment(comment);
    Response.json(result);
});

router.get('/:filter', async (Request, Response) => {
    const filter    = Request.params.filter;
    const comments  = await findUsersComments(filter);
    Response.json(comments);
});

router.get('/last/:user_email', async (Request, Response) => {
    const userEmail = Request.params.user_email;
    const comments  = await getUserLastComment(userEmail);
    Response.json(comments);
});

const getUserLastComment = async (userEmail) => {
    const result = {
        code: 400,
        data: `User last comment doesn't found.`
    };

    try {
        const user = await User.findOne({user_email: userEmail});

        if(user && user._id) {
            const lastComment = await Comment.findOne({user_id: user._id}).sort({created_at: -1});

            if(lastComment) {
                result.code = 200;
                result.data = lastComment.created_at;
            }
        }
    }
    catch (e) {
        console.log(e);
    }

    return result;
};

const storeUser = async (userEmail) => {
    let dbUser;

    try {
        dbUser = await User.findOne({'user_email': userEmail});

        if(!dbUser) {
            const newUser = new User({
                user_email: userEmail,
                gravatar:   md5(userEmail),
                created_at: new Date()
            });

            dbUser = await newUser.save();
        }
    }
    catch(e) {
        dbUser = false;
    }

    return dbUser;
};

const findUsersComments = async (filter) => {
    const result = {
        code: 400,
        data: 'Comments not found.'
    };

    try {
        let comments = [];

        if(filter && filter !== 'all') {
            filter = {
                user_email: new RegExp(filter,'ig'),
                comments: {$ne: []}
            }
        }
        else {
            filter = {
                comments: {$ne: []}
            }
        }

        const dbUsers = await User.aggregate([
            {
                $match: filter
            },
            {
                $lookup: {
                    from: "comments",
                    foreignField: "user_id",
                    localField: "_id",
                    as: "comments"
                }
            }
        ]);

        if(dbUsers) {
            for(let i=0; i<dbUsers.length; i++) {
                for(const [index, comment] of Object.entries(dbUsers[i].comments)) {
                    comments.push({
                        user_email: dbUsers[i].user_email,
                        gravatar: `https://www.gravatar.com/avatar/${dbUsers[i].gravatar}`,
                        text: comment.message
                    });
                }
            }

            if(comments.length) {
                result.code = 200;
                result.data = comments;
            }
        }
    }
    catch (e) {
        console.log(e);
    }

    return result;
};

const storeComment = async (comment) => {
    const result = {
        code: 400,
        data: 'Invalid arguments'
    };

    if(comment.email && comment.content) {
        try {
            const user = await storeUser(comment.email);

            if(user) {
                let newComment = new Comment({
                    user_id: user._id,
                    message: comment.content,
                    created_at: new Date()
                });

                newComment = await newComment.save();

                if (newComment) {
                    result.code = 200;
                    result.data = 'Comment successfully saved!';
                }
            }
            else {
                result.data = "Invalid user email";
            }
        }
        catch (e) {
            result.data = e;
        }
    }

    return result;
};

module.exports = router;
