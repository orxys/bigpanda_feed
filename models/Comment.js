const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

const CommentSchema = new Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    message: String,
    created_at: Date
}, {
    collectionName: 'comments',
    versionKey: false
});

module.exports = mongoose.model('Comment', CommentSchema);