const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

const userSchema = new Schema({
    user_email: {
        type: String,
        required: true,
        unique: true
    },
    gravatar: String,
    created_at: Date
}, {
    collectionName: 'users',
    versionKey: false
});

module.exports = mongoose.model('User', userSchema);